import React from 'react';
import './Header.css';

const Header = () => (
    <nav className="navbar navbar-default">
        <div className="container">
            <div className="navbar-header">
                <ul className="nav navbar-nav">
                    <li><a href="#">Home</a></li>
                </ul>
            </div>
        </div>
    </nav>
);

export default Header;
