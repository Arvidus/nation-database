import React from 'react';
import './Sidebar.css';

import SideNav, {Nav, NavIcon, NavText} from 'react-sidenav';
import store from "../../Store";

export class Sidebar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nameListIsLoaded: false,
            nameListItems: [],
            nameListItemsSortedLoaded: false,
            nameListItemsSorted: [],
            flagListIsLoaded: false,
            flagListItems: [],
            flagListItemsSortedLoaded: false,
            flagListItemsSorted: []
        };
        this.child = React.createRef();
    }

    fetchData() {
        fetch('https://en.wikipedia.org/w/api.php?format=json&action=query&titles=List_of_sovereign_states&prop=revisions&rvprop=content&origin=*')
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        nameListIsLoaded: true,
                        nameListItems: result
                    });
                    this.setNationNames();

                },
                (error) => {
                    this.setState({
                        nameListIsLoaded: false,
                        error
                    });
                }
            );
        fetch('https://en.wikipedia.org/w/api.php?format=json&action=query&generator=images&gimlimit=max&prop=imageinfo&iiprop=url&titles=List_of_sovereign_states&origin=*')
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        flagListIsLoaded: true,
                        flagListItems: result
                    });
                    this.findNationFlags();

                },
                (error) => {
                    this.setState({
                        flagListIsLoaded: false,
                        error
                    });
                }
            )
    }

    componentDidMount() {
        this.fetchData();

    }

    findNationFlags() {

        let flagObject;
        let flagObjectId;
        let nameObject;
        let sortObject = [];

        if (this.state.nameListItemsSortedLoaded) {

            flagObject = this.state.flagListItems.query.pages;
            nameObject = this.state.nameListItemsSorted;

            for (let i = 0; i < nameObject.length; i++) {
                for (let j = 0; j < Object.keys(flagObject).length; j++) {

                    flagObjectId = Object.keys(flagObject)[j];

                    if (nameObject[i] === "Guinea") {

                        if (flagObject[flagObjectId].title.includes("Equatorial") || flagObject[flagObjectId].title.includes("Bissau")) {
                            continue;
                        }
                    }
                    if (nameObject[i] === "Samoa") {

                        if (flagObject[flagObjectId].title.includes("American")) {
                            continue;
                        }
                    }
                    if (nameObject[i] === "Sudan") {

                        if (flagObject[flagObjectId].title.includes("South")) {
                            continue;
                        }
                    }

                    // Flags and names pairing - Exceptions - API-inconsistencies

                    if (nameObject[i] === "Korea North" && flagObject[flagObjectId].title.includes("North Korea")) {
                        sortObject.push(flagObject[flagObjectId].imageinfo[0].url);

                        break;
                    }
                    if (nameObject[i] === "Korea South" && flagObject[flagObjectId].title.includes("South Korea")) {
                        sortObject.push(flagObject[flagObjectId].imageinfo[0].url);

                        break;
                    }
                    if (nameObject[i] === "SADR" && flagObject[flagObjectId].title.includes("Sahrawi Arab Democratic Republic")) {

                        sortObject.push(flagObject[flagObjectId].imageinfo[0].url);

                        break;
                    }
                    if (nameObject[i] === "Taiwan" && flagObject[flagObjectId].title.includes("Republic of China")) {

                        if (flagObject[flagObjectId].title.includes("People's")) {
                            continue;
                        }

                        sortObject.push(flagObject[flagObjectId].imageinfo[0].url);

                        break;
                    }
                    if (nameObject[i] === "Ivory Coast" && flagObject[flagObjectId].title.includes("Côte d'Ivoire")) {
                        sortObject.push(flagObject[flagObjectId].imageinfo[0].url);

                        break;
                    }

                    //  Flags and names pairing - Standard
                    if (flagObject[flagObjectId].title.includes(nameObject[i])) {

                        sortObject.push(flagObject[flagObjectId].imageinfo[0].url);

                        break;
                    }
                }
            }
            this.setState({
                flagListItemsSortedLoaded: true,
                flagListItemsSorted: sortObject
            });
            // console.log(sortObject);
        }

    }

    findNationNames(str) {

        let pattern1 = /id="\b[a-zA-Z_ ]{1,}\S+?\b">/g;
        let result = str.match(pattern1);

        let index;
        for (let i = 0; i < result.length; i++) {
            index = result[i].replace('id="', '').replace('">', '');
            result[i] = index;
        }
        return result;
    }

    setNationNames() {
        let testObject;
        let objectId;
        let listContent;
        let isLoaded = false;

        if (this.state.nameListIsLoaded) {
            objectId = Object.keys(this.state.nameListItems.query.pages)[0];
            testObject = this.state.nameListItems.query.pages;
            listContent = testObject[objectId].revisions[0]['*'].toString();

            listContent = this.findNationNames(listContent);
            isLoaded = true;

            this.setState({
                nameListItemsSortedLoaded: true,
                nameListItemsSorted: listContent
            });
            console.log(this.state.nameListItemsSorted);
        }
    }

    sendName(name) {
        // Address name exceptions:
        switch (name) {
            case "Bahamas":
                name = "The_Bahamas";
                break;
            case "Gambia":
                name = "The_Gambia";
                break;
            case "Georgia":
                name = "Georgia_(country)";
                break;
            case "Ireland":
                name = "Republic_of_Ireland";
                break;
            case "Korea North":
                name = "North_Korea";
                break;
            case "Korea South":
                name = "South_Korea";
                break;
            case "Macedonia":
                name = "Republic_of_Macedonia";
                break;
            case "Micronesia":
                name = "Federated_States_of_Micronesia";
                break;
            case "Palestine":
                name = "State_of_Palestine";
                break;
            case "Artsakh":
                name = "Republic_of_Artsakh";
                break;
            case "SADR":
                name = "Sahrawi_Arab_Democratic_Republic";
        }
        store.dispatch({type: "NAT", payload: name});
        document.querySelector('.homeDiv').scrollTop = 0;
    }

    render() {
        const {
            nameListIsLoaded,
            nameListItems,
            nameListItemsSortedLoaded,
            nameListItemsSorted,
            flagListItems,
            flagListIsLoaded,
            flagListItemsSortedLoaded,
            flagListItemsSorted
        } = this.state;

        return (
            <div className='SidebarDiv' style={{background: '#cc6d70', color: '#FFF', width: 220}}>
                <SideNav highlightColor='#cc6d70' highlightBgColor='#FFF' selected='clicked'>
                    {
                        flagListItemsSortedLoaded ? nameListItemsSorted.map((name, i) => {
                            return <Nav onNavClick={() => {
                                this.sendName(name)
                            }} key={name + 'Btn'} className='SidebarBtn'>
                                <NavIcon><img height={15} src={flagListItemsSorted[i]}/></NavIcon>
                                <NavText> {name} </NavText>
                            </Nav>
                        }) : null
                    }
                </SideNav>
            </div>

        );
    }
}

