import React, {Component} from 'react';
import { createStore } from 'redux';
import PropTypes from 'prop-types';

import store from "../../Store";

import './Home.css';

// Temp HTML Fetch
// https://en.wikipedia.org/w/api.php?action=parse&page=Sweden&format=json&origin=*

export class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nationUrl: '',
            pageContent: '',
            pageContentLoaded: false
        };
    }

    fetchData(titles){
        fetch('https://en.wikipedia.org/w/api.php?action=parse&page=' + titles + '&format=json&origin=*')
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        pageContentLoaded: true,
                        pageContent: result.parse.text['*']
                    });
                },
                (error) => {
                    this.setState({
                        pageContentLoaded: false,
                        error
                    });
                }
            )
    }

    loadNation(nationName){
        this.fetchData(nationName);
    }

    componentDidMount() {
        this.loadNation('Sweden');
        store.subscribe(() => {
            this.loadNation(store.getState());
        });
    }

    render() {

        const {
            nationUrl,
            pageContent,
            pageContentLoaded
        } = this.state;
        return (
            <div className="homeDiv" dangerouslySetInnerHTML={{__html: pageContent.toString() }}>
            </div>
        );
    }
}