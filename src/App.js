import React, {Component} from 'react';
import './App.css';

// import store from 'store';

import Header from './components/Header/Header';
import {Home} from './components/Home/Home';
import {Sidebar} from "./components/Sidebar/Sidebar";


import {Provider} from "react-redux";
import store from "./Store";


// --- Nation list URL ---
//https://en.wikipedia.org/w/api.php?format=json&action=query&titles=List_of_sovereign_states&prop=revisions&rvprop=content&origin=*
// --- Nation Flag Thumbnail List ---
//https://en.wikipedia.org/w/api.php?format=json&action=query&generator=images&gimlimit=max&prop=imageinfo&iiprop=url&titles=List_of_sovereign_states



class App extends React.Component {

    render() {
        return (
            <Provider store={store}>
                <div className="App">
                    <Sidebar/>
                    <Home/>
                </div>
            </Provider>
        )
    }
}

/*const mapStateToProps = state => ({
    homeUrl: state.homeUrl
})*/

export default App;
