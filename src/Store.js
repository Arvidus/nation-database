import createStore from "redux/src/createStore";

const reducer = function (state, action) {
    return action.payload;
};

const store = createStore(reducer, "");

store.subscribe(() => {
    console.log("store changed " + store.getState())
});

export default store;